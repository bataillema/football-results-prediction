# Football results prediction

Prediction model based on Corentin Herbinet's paper : Predicting Football Results Using Machine Learning Techniques

DataFrames have been uploaded on the kaggle Database : European Soccer Database ==> https://www.kaggle.com/hugomathien/soccer
database.sqlite is too large to be loaded : please download these data following the link above

More information are available on the paper